<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class buku_tamu extends MX_Controller {

	/**
	 * @author : Gede Lumbung
	 * @web : http://gedelumbung.com
	 **/
 
   public function index($uri=0)
   {
       
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Buku Tamu", '/');
			
			$d['aktif_artikel_sekolah'] = "";
			$d['aktif_galeri_sekolah'] = "";
			$d['aktif_berita'] = "";
			$d['aktif_pengumuman'] = "";
			$d['aktif_agenda'] = "";
			$d['aktif_buku_tamu'] = "active";
			$d['aktif_list_download'] = "";
			
			$filter['nama'] = $this->session->userdata("by_nama");
			$d['data_retrieve'] = $this->app_global_superadmin_model->generate_index_buku_tamu($this->config->item("limit_item"),$uri,$filter);
			
			$this->load->view('bg_header',$d);
			$this->load->view('buku_tamu/bg_home');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("auth/user_login");
		}
   }
 
	public function set()
	{
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
			$sess['by_nama'] = $this->input->post("by_judul");
			$this->session->set_userdata($sess);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
 
	public function hapus($id_param)
	{

		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{

			$where['id_super_buku_tamu'] = $id_param;
			$this->db->delete("dlmbg_super_buku_tamu",$where);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
 
	public function approve($id_param,$value)
	{

		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{


			//var_dump($id_param)or die();
			$id['id_super_buku_tamu'] = $id_param;
			$up['stts'] = $value;
			$this->db->update("dlmbg_super_buku_tamu",$up,$id);
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("web");
		}
   }
   
   public function edit($id_param)
   {
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
                 
			$this->breadcrumb->append_crumb('Dashboard', base_url().'superadmin');
			$this->breadcrumb->append_crumb("Buku Tamu", base_url().'superadmin/buku_tamu');
			$this->breadcrumb->append_crumb("Reply Buku Tamu", '/');
			
			$d['aktif_artikel_sekolah'] = "";
			$d['aktif_galeri_sekolah'] = "";
			$d['aktif_berita'] = "";
			$d['aktif_pengumuman'] = "";
			$d['aktif_agenda'] = "active";
			$d['aktif_buku_tamu'] = "";
			$d['aktif_list_download'] = "";
			
			$where['id_super_buku_tamu'] = $id_param;
		
                       // var_dump($where)or die();
                        $query_str="SELECT A.id_super_buku_tamu,A.nama,A.kontak,A.pesan,A.tanggal,A.stts, 
                                    (SELECT dlmbg_super_buku_tamu_reply.reply__konten FROm dlmbg_super_buku_tamu_reply WHERE dlmbg_super_buku_tamu_reply.reply__id_bukutamu=A.id_super_buku_tamu) as reply
                                    FROM dlmbg_super_buku_tamu A WHERE A.id_super_buku_tamu = $id_param";

                        $query=$this->db->query($query_str);

                        // fetch one row data
                        $get=$query->row();

                        if(!empty($get->nama)){
                                $d['judul'] = $get->nama;
                                $d['isi'] = $get->pesan;
                                $d['kontak'] = $get->kontak;
                                $d['balas']=$get->reply;
                                $d['id_param'] = $get->id_super_buku_tamu;
                                $d['tipe'] = "edit";
                        }else
                        {
//                             var_dump('bb')or die();
                                $d['judul'] = $get->nama;
                                $d['isi'] = $get->pesan;
                                $d['kontak'] = $get->kontak;
                                $d['balas']=$get->reply;
                                $d['id_param'] = $get->id_super_buku_tamu;
                                $d['tipe'] = "edit";
                            
                        }
			
			
			$this->load->view('bg_header',$d);
			$this->load->view('buku_tamu/bg_input');
			$this->load->view('bg_footer');
		}
		else
		{
			redirect("auth/user_login");
		}
   }


 
   public function reply()
   {
		if($this->session->userdata("logged_in")!="" && $this->session->userdata("tipe_user")=="superadmin")
		{
                    $tipe = $this->input->post("tipe");
                     $id_reply = $this->input->post("id_param");
                    
               

                    $query_str="SELECT
                        A.reply__id,A.reply__id_bukutamu,A.reply__konten, A.reply__create_at,A.reply__update_at,A.reply__user__create,A.reply__user__update,B.id_super_buku_tamu,B.nama,B.kontak,B.pesan,
                        B.tanggal,B.stts FROM dlmbg_super_buku_tamu_reply A inner join dlmbg_super_buku_tamu  B ON A.reply__id_bukutamu=B.id_super_buku_tamu where A.reply__id_bukutamu=".$id_reply."";

                        $query=$this->db->query($query_str);
                       // print_r($query)or die();
                        $get=$query->row();
                        $konten_cek=  $get->reply__konten;
                     
                 
                       
			if(empty($konten_cek))
			{
				$in['reply__id_bukutamu'] = $this->input->post("id_param");
				$in['reply__konten'] = $this->input->post("reply");
                                $in['reply__create_at']=time()+3600*7;
                                $in['reply__update_at']=time()+3600*7;
                                $in['reply__user__create']=$this->session->userdata("id_admin_super");
                                $in['reply__user__update']=$this->session->userdata("id_admin_super");
				
				$this->db->insert("dlmbg_super_buku_tamu_reply",$in);
                                $this->session->set_flashdata('result_insert', 'Data berhasil masuk');
			}
			else 
			{
                         $data = array('reply__konten' => $this->input->post("reply"));
                         $this->db->where('reply__id_bukutamu', $id_reply);
                           $this->db->set('reply__update_at', 'NOW()', FALSE);
                         $this->db->update('dlmbg_super_buku_tamu_reply', $data); 
                       
                         $this->session->set_flashdata("simpan_akun","Password lama tidak cocok");
			}
                        
			redirect("superadmin/buku_tamu");
		}
		else
		{
			redirect("auth/user_login");
		}
   }
   
}


 
/* End of file superadmin.php */
