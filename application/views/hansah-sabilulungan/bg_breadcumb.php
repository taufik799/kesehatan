<?php


$data_api='http://simpus.bandungkab.go.id/api_jml_kunj.php';

if(!empty($data_api)){

$url =$data_api; // path to your JSON file
$data = file_get_contents($url); // put the contents of the file into a variable
$characters = json_decode($data,true); // decode the JSON feed

//var_dump($characters['dilayani']!='')or die();
if(!empty($characters['kunj_hari']))
                        {$kunjungan_hari    =$characters['kunj_hari'];}else{$kunjungan_hari=0;}
if($characters['kunj_bulan'] !='')
                         {$kunjungan_bulan   =$characters['kunj_bulan'];}else{$kunjungan_bulan=0;}
if($characters['kunj_tahun']!='')
                        {$kunjungan_tahun    =$characters['kunj_tahun'];}else{$kunjungan_tahun=0;}
        
 $bpjs_dilayani      = $characters['dilayani'];
 
 
  $persentase    = round(($characters['persentase']),2);
  

                       
 if(!empty($bpjs_dilayani))
 {
       $bpjs_dilayani      = $characters['dilayani'];
 }else
 {
      $bpjs_dilayani      = 'Data Kosong';
 }
 //var_dump($characters['dilayani'])or die();

$kjg_hari=$kunjungan_hari['kunj_hari'];
        if(!empty($kunjungan_hari))
            { $jml_hari=$kjg_hari;}
        else
            {$jml_hari='Data Kosong';}
         
$kjg_bln=$kunjungan_bulan['kunj_bulan'];
        if(!empty($kunjungan_bulan))
            { $jml_bln=$kjg_bln;}
        else
            {$jml_bln='Data Kosong';}         
         
         
 $kjg_tahun=$kunjungan_tahun['kunj_tahun'];
        if(!empty($kunjungan_tahun))
            { $jml_thn=$kjg_tahun;}
        else
            {$jml_thn='Data Kosong';}        
            

$hariini = date('d-m-Y');
$bulanini = date('m');
$tahunini= date('Y');


$dateObj   = DateTime::createFromFormat('!m', $bulanini);
$monthName = $dateObj->format('F'); // March
?>
<ul>
	<div id="result" style="color:red"></div>
		
	</ul>
<?php echo $this->breadcrumb->output(); ?>
<div class="cleaner_h20"></div>
<!-- /.row -->
 
           
                   <span  align="center"><h3 style="color:#069"><?PHP echo strtoupper('Informasi Pelayanan kesehatan di puskesmas di kabupaten bandung');?></h3></span>
             
                   <div class="panel-heading" align="center">
               
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?PHP echo number_format($kjg_hari); ?> </div>
                                    <div></div>
                                    
                                </div>
                            </div>
                        </div>
                        <a href="http://simpus.bandungkab.go.id/portal_index.php" target="_blank">
                            <div class="panel-footer">
                                <span class="pull-left" align="center"><?PHP echo strtoupper('KUNJUNGAN PUSKESMAS');?> <br>TGL<?PHP echo strtoupper($hariini); ?> </span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?PHP echo number_format($kjg_bln); ?> </div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                        <a href="http://simpus.bandungkab.go.id/portal_index.php" target="_blank">
                            <div class="panel-footer">
                                <span class="pull-left" align="center">KUNJUNGAN PUSKESMAS BULAN <?PHP echo strtoupper($monthName); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?PHP echo  number_format($kjg_tahun);; ?></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                        <a href="http://simpus.bandungkab.go.id/portal_index.php" target="_blank">
                            <div class="panel-footer">
                                <span class="pull-left">KUNJUNGAN PUSKESMAS TAHUN <?PHP echo $tahunini;?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
               
                <div class="col-lg-2 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"></div>
                                    <div><?PHP echo  number_format($characters['dilayani']).'('.$persentase.'%)' ;  ?> </div>
                                </div>
                            </div>
                        </div>
                        <a href="http://simpus.bandungkab.go.id/portal_index.php" target="_blank">
                            <div class="panel-footer">
                                <span class="pull-left">KONTAK RATE KUNJUNGAN PUSKESMAS  BPJS</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
                   
                  
<div class="cleaner_h20"></div>

<?PHP
}?>